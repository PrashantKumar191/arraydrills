function find(element, cb) {
  // If te arguments are not array and function.
  if (typeof element !== "object" || typeof cb !== "function") {
    return;
  }

  // If the arguments are null.
  if (element === null || cb === null) {
    return [];
  }

  // calling callback function on each element of array
  for (let i = 0; i < element.length; i++) {
    let a = cb(element[i], i, element);
    if (a == true) {
      return element[i];
    }
  }
  return;
}
module.exports = find;
