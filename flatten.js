const flatten  = function(element,depth =1,out=[]){
    
    if(element){
        for(let i of element){
            if(i){
                if(Array.isArray(i)  && depth >=0){
                    depth--;
                    flatten(i,depth,out);
                    
                }else{
                    out.push(i);
                }
            }
            
        }
        return out;
    }else{
        return [];
    }
}

module.exports = flatten;