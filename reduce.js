function reduce(element, cb, startingValue) {
  // If the passed arguments are not array and function.
  if (typeof element !== "object" || typeof cb !== "function") {
    return [];
  }

  //If the passed arguments ar null.
  if (element === null || cb === null) {
    return [];
  }

  //  calling callback function on each element of array.
  let result;
  if (typeof startingValue === "undefined") {
    startingValue = element[0];
    for (let i = 1; i < element.length; i++) {
      result = cb(startingValue, element[i], i, element);
      startingValue = result;
    }
  } else {
    for (let i = 0; i < element.length; i++) {
      result = cb(startingValue, element[i], i, element);
      startingValue = result;
    }
  }

  return result;
}

module.exports = reduce;
