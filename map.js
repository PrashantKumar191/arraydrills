function map(element, cb) {
  if (typeof element !== "object" || typeof cb !== "function") {
    return [];
  }

  if (element === null || cb === null) {
    return [];
  }

  let resultArr = [];
  let cbReturnValue;

  for (let i = 0; i < element.length; i++) {
    cbReturnValue = cb(element[i], i, element);

    resultArr.push(cbReturnValue);
  }
  return resultArr;
}

module.exports = map;
