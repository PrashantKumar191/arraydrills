function each(element, cb) {
  // If passed arguments are not array and function.

  if (typeof element !== "object" || typeof cb !== "function") {
    return;
  }

  // If the passed arguments are null.
  if (element === null || cb === null) {
    return [];
  }

  // Calling callback function on each element.
  for (let i = 0; i < element.length; i++) {
    cb(element[i], i, element);
  }
  return;
}

module.exports = each;
