function filter(element, cb) {
  // Checking if the passed arguments are not array and function.
  if (typeof element !== "object" || typeof cb !== "function") {
    return [];
  }

  // If the passed arguments ar null.
  if (element === null || cb === null) {
    return;
  }

  // Calling the callback function on each element of array.
  let result = [];

  for (let i = 0; i < element.length; i++) {
    let cbReturnValue = cb(element[i], i, element);

    if (cbReturnValue === true && typeof cbReturnValue !== "undefined") {
      result.push(element[i]);
    }
  }
  return result;
}

module.exports = filter;
